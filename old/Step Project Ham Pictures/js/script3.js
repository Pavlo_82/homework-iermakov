// $( document ).ready(function () {
//     $(".moreBox").slice(0, 3).show();
//     if ($(".our-work-item-img:hidden").length != 0) {
//         $("#loadMore").show();
//     }
//     $("#loadMore").on('click', function (e) {
//         e.preventDefault();
//         $(".moreBox:hidden").slice(0, 12).slideDown();
//         if ($(".moreBox:hidden").length == 0) {
//             $("#loadMore").fadeOut('slow');
//         }
//     });
// });

// $('.our-work-nav-item').on('click', function(e) {
//     console.dir(e.target)
//     $('.our-work-nav-list li').removeClass('active');
//     $(this).addClass('active'); // выделяем выбранную категорию
//
//     const allWorks = $(this).attr('data-filter'); // определяем категорию
//
//     if (allWorks === 'all') { // если all
//         $('.our-work-list-img li').show(); // отображаем все позиции
//     } else { // если не all
//         $('.our-work-list-img li').hide(); // скрываем все позиции
//         $('.our-work-list-img li[data-filter="' + allWorks + '"]').show(showImage (e)); // и отображаем позиции из соответствующей категории
//     }
// });
//
// $( document ).ready(function () {
//     $(".moreBox").slice(0, 3).show();
//     if ($(".our-work-item-img:hidden").length != 0) {
//         $("#loadMore").show();
//     }
//     $("#loadMore").on('click', e => showImage(e));
// });
// function showImage (e) {
//     e.preventDefault();
//     $(".moreBox:hidden").slice(0, 6).slideDown();
//     if ($(".moreBox:hidden").length == 0) {
//         $("#loadMore").fadeOut('slow');
//     }
// };

const controls = document.querySelectorAll('.people-say-item-span');
for(var i=0; i<controls.length; i++){
    controls[i].style.display = 'inline-block';
}

const slides = document.querySelectorAll('#slides .people-say-item');
const currentSlide = 0;
// var slideInterval = setInterval(nextSlide,2000);

function nextSlide(){
    goToSlide(currentSlide+1);
}

function previousSlide(){
    goToSlide(currentSlide-1);
}

function goToSlide(n){
    const currentSlide = (n+slides.length)%slides.length;
    slides[currentSlide].className = 'people-say-item';
    slides[currentSlide].className = 'people-say-item showing';
}


// const playing = true;
// var pauseButton = document.getElementById('pause');

// function pauseSlideshow(){
//     pauseButton.innerHTML = '&#9658;'; // play character
//     playing = false;
//     clearInterval(slideInterval);
// }

// function playSlideshow(){
//     pauseButton.innerHTML = '&#10074;&#10074;'; // pause character
//     playing = true;
//     slideInterval = setInterval(nextSlide,2000);
// }

// pauseButton.onclick = function(){
//     if(playing){ pauseSlideshow(); }
//     else{ playSlideshow(); }
// };

const next = document.getElementById('next');
const previous = document.getElementById('previous');

next.onclick = function(){
    nextSlide();
};
previous.onclick = function(){
    previousSlide();
};