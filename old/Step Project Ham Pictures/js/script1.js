$('.our-work-nav-item').on('click', function(e) {
    $('.our-work-nav-list li').removeClass('active');
    $(this).addClass('active'); // выделяем выбранную категорию

    const allWorks = $(this).attr('data-filter'); // определяем категорию

    if (allWorks === 'all') { // если all
        $('.our-work-list-img li').hide();
        showImage();
    } else { // если не all
        $('.our-work-list-img li').hide(); // скрываем все позиции
        $('.our-work-list-img li[data-filter="' + allWorks + '"]').show(); // и отображаем позиции из соответствующей категории
    }
});

$( document ).ready(function () {
    $("#loadMore").on('click', e => showImage(e));
});
function showImage () {
    $(".our-work-item-img:hidden").slice(0, 12).slideDown();
    if ($(".our-work-item-img:hidden").length == 0) {
        $("#loadMore").fadeOut('slow');
    }
};
