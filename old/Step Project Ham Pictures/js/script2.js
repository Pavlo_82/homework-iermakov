const nextBtn = document.getElementById("sliderNext");
const previousBtn = document.getElementById("sliderPrev");
const imgBtn = [...document.querySelectorAll(".people-say-item-photo-small")];
const slides = document.querySelectorAll(".people-say-item");
let current = 0;
function slider(){
    for (let i=0; i<slides.length; i++){
        slides[i].classList.add(".hidden");
        slides[current].classList.remove(".hidden");
    }
}
slider();
nextBtn.onclick = function (){
    if (current -1 === -1){
        current = slides.length -1;
    }
    else {
        current --;
    }
    slider()
};
previousBtn.onclick = function (){
    if (current +1 === slides.length){
        current = 0;
    }
    else {
        current ++;
    }
    slider()
};
imgBtn.forEach(elem => elem.addEventListener("click", toggleSlide));
function toggleSlide(e) {
    e.target.closest("ul").querySelector(".active").classList.remove("active");
    e.target.closest("li").classList.add("active");
    const index = imgBtn.findIndex(elem => elem.classList.contains("active"));
    showSlide(index)
}
function showSlide(index) {
    slides.forEach(elem => elem.classList.add("hidden"));
    slides[index].classList.remove("hidden");
}
// nextBtn.onclick = function () {
//     plusSlide()
// };
// function plusSlide() {
//     goToSlide(currentSlide+1);
// }
// previousBtn.onclick = function () {
//     minusSlide()
// };
// function minusSlide() {
//     goToSlide(currentSlide-1);
// }
// function goToSlide(n) {
//     const active = document.querySelector(".showing");
//     active.classList.remove('showing');
//     slides[currentSlide].classList.add("showing");
//     console.log(n);
//     // currentSlide = (n+slides.length)%slides.length;
// }
