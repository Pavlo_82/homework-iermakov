const modalElement = document.querySelector('#enter-container');
const parentCard = document.querySelector('#cards');
const searchParentContainer = document.querySelector('#search-container');
const buttonEnter = document.querySelector('#enter-Btn');
const editContainer = document.querySelector('#editCard')
const baseUrls = {
    baseUrl: 'https://ajax.test-danit.com/api/',
    baseUrlLogin: 'login',
    baseUrlCards: 'v2/cards/',
}
// const user = {
//     email: 'ptopdruk@gmail.com',
//     password: 'Pavlo04051982'
// }

const inputInfoMail = {
    inputName: {
        inputName: 'email',
    },
    inputPlaceholder: {
        inputPlaceholder: 'Введите ваш е-мейл',
    },
    inputClasses: {
        inputClassesMb3: 'mb-3',
        inputClassesCol: 'col-form-label',
        inputClassesFormControl: 'form-control'
    },
    inputId: {
        inputId: 'email',
    },
    inputAtribute: {
        atributeFor: 'for',
        atributeForText: 'email',
        atributeRequired: 'required',
        atributeRequiredText: true,
    }
}

const inputInfoPass = {
    inputName: {
        inputName: 'password'
    },
    inputPlaceholder: {
        inputPlaceholder: 'Введите ваш пароль'
    },
    inputClasses: {
        inputClassesMb3: 'mb-3',
        inputClassesCol: 'col-form-label',
        inputClassPass: 'password'
    },
    inputId: {
        inputId: 'password',
    },
    inputAtribute: {
        atributeFor: 'for',
        atributeForText: 'password',
        atributeType: 'type',
        atributeTypeText: 'password',
        atributeRequired: 'required',
        atributeRequiredText: true,
    }
}

const inputInfoSearch = {
    inputName: {
        inputName: 'search'
    },
    inputPlaceholder: {
        inputPlaceholder: 'doctor'
    },
    inputClasses: {
        inputClassesMb3: 'mb-3',
        inputClassesFormControl: 'form-control',
    },
    inputId: {
        inputId: 'search',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'text',
    }
}

const inputInfoCheckOpen = {
    inputName: {
        inputName: 'open'
    },
    inputPlaceholder: {
        inputPlaceholder: 'open'
    },
    inputClasses: {
        inputClassForm: 'form-check',
        inputClassFormControl: 'form-check-inline',
        inputClassFormCheck: 'form-check-input',
        inputClassCheckLabel: 'form-check-label'
    },
    inputId: {
        inputId: 'open',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'checkbox',
        atributeValue: 'value',
        atributeValueText: 'option1',
        atributeFor: 'for',
        atributeForText: 'open',
        atributeRequired: 'required',
        atributeRequiredText: true,
    }
}

const inputInfoCheckDone = {
    inputName: {
        inputName: 'done'
    },
    inputPlaceholder: {
        inputPlaceholder: 'done'
    },
    inputClasses: {
        inputClassForm: 'form-check',
        inputClassFormControl: 'form-check-inline',
        inputClassFormCheck: 'form-check-input',
        inputClassCheckLabel: 'form-check-label'
    },
    inputId: {
        inputId: 'done',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'checkbox',
        atributeValue: 'value',
        atributeValueText: 'option1',
        atributeFor: 'for',
        atributeForText: 'done'
    }
}

const inputInfoRememberMe = {
    inputName: {
        inputName: 'remember'
    },
    inputPlaceholder: {
        inputPlaceholder: 'Запомнить'
    },
    inputClasses: {
        inputClassForm: 'form-check',
        inputClassFormCheck: 'form-check-input',
        inputClassCheckLabel: 'form-check-label'
    },
    inputId: {
        inputId: 'remember-me',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'checkbox',
        atributeHtmlFor: 'htmlFor',
        atributeHtmlForText: 'remember-me'
    }
}

const inputInfoPurposeVisit = {
    inputName: {
        inputName: 'title'
    },
    inputPlaceholder: {
        inputPlaceholder: 'Цель визита'
    },
    inputClasses: {
        inputClassForm: 'form-group',
        inputClassFormControl: 'form-control',
    },
    inputId: {
        inputId: 'purpose',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'text',
        atributeFor: 'for',
        atributeForText: 'purpose',
        atributeRequired: 'required',
        atributeRequiredText: true,
    }
}

const inputInfoIndexBody = {
    inputName: {
        inputName: 'bodyMassIndex'
    },
    inputPlaceholder: {
        inputPlaceholder: 'Индекс массі тела'
    },
    inputClasses: {
        inputClassForm: 'form-group',
        inputClassFormControl: 'form-control',
    },
    inputId: {
        inputId: 'Index-body',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'text',
        atributeFor: 'for',
        atributeForText: 'purpose',
        atributeRequired: 'required',
        atributeRequiredText: true,
    }
}

const inputInfoNormalPressure = {
    inputName: {
        inputName: 'normalPressure'
    },
    inputPlaceholder: {
        inputPlaceholder: 'Нормальное давление'
    },
    inputClasses: {
        inputClassForm: 'form-group',
        inputClassFormControl: 'form-control',
    },
    inputId: {
        inputId: 'normal-pressure',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'text',
        atributeFor: 'for',
        atributeForText: 'purpose',
        atributeRequired: 'required',
        atributeRequiredText: true,
    }
}

const inputInfoAge = {
    inputName: {
        inputName: 'age'
    },
    inputPlaceholder: {
        inputPlaceholder: 'Возвраст'
    },
    inputClasses: {
        inputClassForm: 'form-group',
        inputClassFormControl: 'form-control',
    },
    inputId: {
        inputId: 'age',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'text',
        atributeFor: 'for',
        atributeForText: 'purpose',
        atributeRequired: 'required',
        atributeRequiredText: true,
    }
}

const inputInfoFullName = {
    inputName: {
        inputName: 'patientFullName'
    },
    inputPlaceholder: {
        inputPlaceholder: 'Фамилия Имя Отчество'
    },
    inputClasses: {
        inputClassForm: 'form-group',
        inputClassFormControl: 'form-control',
    },
    inputId: {
        inputId: 'FullName',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'text',
        atributeFor: 'for',
        atributeForText: 'purpose',
        atributeRequired: 'required',
        atributeRequiredText: true,
    }
}

const inputInfoLastDate = {
    inputName: {
        inputName: 'dateLastVisit'
    },
    inputPlaceholder: {
        inputPlaceholder: 'Последняя дата визита'
    },
    inputClasses: {
        inputClassForm: 'form-group',
        inputClassFormControl: 'form-control',
    },
    inputId: {
        inputId: 'LastDate',
    },
    inputAtribute: {
        atributeType: 'type',
        atributeTypeText: 'text',
        atributeFor: 'for',
        atributeForText: 'purpose',
        atributeRequired: 'required',
        atributeRequiredText: true,
    }
}

class Input {
    constructor({inputName, inputPlaceholder, inputClasses, inputId, inputAtribute}) {
        this.inputName = inputName;
        this.inputPlaceholder = inputPlaceholder;
        this.inputClasses = inputClasses;
        this.inputAtribute = inputAtribute;
        this.inputId = inputId;
        this.createInputMail();
        this.createInputPass();
        this.createInput();
        this.createInputSearch();
        this.createInputCheck();
    }

    createInputMail() {
        const formDiv = document.createElement('div');
        const formLabel = document.createElement('label');
        const formInput = document.createElement('input');
        const {inputName} = this.inputName;
        const {inputPlaceholder} = this.inputPlaceholder;
        const {
            inputClassesMb3,
            inputClassesCol,
            inputClassesFormControl,
        } = this.inputClasses
        const {inputId} = this.inputId
        const {
            atributeFor,
            atributeForText,
            atributeRequired,
            atributeRequiredText,
        } = this.inputAtribute

        formDiv.classList.add(inputClassesMb3)

        formLabel.classList.add(inputClassesCol)
        formLabel.setAttribute(atributeFor, atributeForText)

        formLabel.innerHTML = inputPlaceholder;
        formInput.id = inputId

        formDiv.appendChild(formLabel)

        formInput.setAttribute('name', inputName)
        formInput.classList.add(inputClassesFormControl)
        formInput.setAttribute(atributeFor, atributeForText);
        formInput.setAttribute(atributeRequired, atributeRequiredText,)
        formDiv.appendChild(formInput)

        return formDiv
    }

    createInputPass() {
        const formDiv = document.createElement('div');
        const formLabel = document.createElement('label');
        const formInput = document.createElement('input');
        const {inputName} = this.inputName;
        const {inputPlaceholder} = this.inputPlaceholder;
        const {
            inputClassesMb3,
            inputClassesCol,
            inputClassPass
        } = this.inputClasses
        const {inputId,} = this.inputId
        const {
            atributeFor,
            atributeForText,
            atributeType,
            atributeTypeText,
            atributeRequired,
            atributeRequiredText,
        } = this.inputAtribute

        formDiv.classList.add(inputClassesMb3)

        formLabel.classList.add(inputClassesCol)
        formLabel.setAttribute(atributeFor, atributeForText)

        formLabel.innerHTML = inputPlaceholder;
        formInput.id = inputId;

        formDiv.appendChild(formLabel)

        formInput.setAttribute('name', inputName)
        formInput.classList.add(inputClassPass)
        formInput.setAttribute(atributeType, atributeTypeText);
        formInput.setAttribute(atributeRequired, atributeRequiredText,)
        formDiv.appendChild(formInput)

        return formDiv
    }

    createInputSearch() {
        const formDiv = document.createElement('div');
        const formInput = document.createElement('input');
        const {inputName} = this.inputName;
        const {inputPlaceholder} = this.inputPlaceholder;
        const {
            inputClassesMb3,
            inputClassesFormControl,
        } = this.inputClasses
        const {inputId} = this.inputId
        const {
            atributeType,
            atributeTypeText,
        } = this.inputAtribute

        formDiv.classList.add(inputClassesMb3)

        formInput.placeholder = inputPlaceholder;
        formInput.id = inputId
        // this.formInput.setAttribute('name', inputName)
        formInput.classList.add(inputClassesFormControl)
        formInput.setAttribute(atributeType, atributeTypeText);
        formDiv.appendChild(formInput)
        formInput.setAttribute('name', inputName)

        return formDiv
    }

    createInputCheck() {
        const formDiv = document.createElement('div');
        const formLabel = document.createElement('label');
        const formInput = document.createElement('input');
        const {inputName} = this.inputName;
        const {inputPlaceholder} = this.inputPlaceholder;
        const {
            inputClassForm,
            inputClassFormControl,
            inputClassFormCheck,
            inputClassCheckLabel,
        } = this.inputClasses
        const {inputId,} = this.inputId
        const {
            atributeType,
            atributeTypeText,
            atributeValue,
            atributeValueText,
            atributeFor,
            atributeForText
        } = this.inputAtribute

        formDiv.classList.add(inputClassForm);
        formDiv.classList.add(inputClassFormControl);

        formLabel.classList.add(inputClassCheckLabel)
        formLabel.setAttribute(atributeFor, atributeForText)

        formLabel.innerHTML = inputPlaceholder;
        formInput.id = inputId;

        formDiv.appendChild(formLabel)

        formInput.setAttribute('name', inputName)
        formInput.classList.add(inputClassFormCheck)
        formInput.setAttribute(atributeType, atributeTypeText);
        formInput.setAttribute(atributeValue, atributeValueText);
        formDiv.appendChild(formInput)

        return formDiv
    }

    createInputCheckRemember() {
        const formDiv = document.createElement('div');
        const formLabel = document.createElement('label');
        const formInput = document.createElement('input');
        const {inputName} = this.inputName;
        const {inputPlaceholder} = this.inputPlaceholder;
        const {
            inputClassForm,
            inputClassFormCheck,
            inputClassCheckLabel,
        } = this.inputClasses
        const {inputId,} = this.inputId
        const {
            atributeType,
            atributeTypeText,
            atributeHtmlFor,
            atributeHtmlForText,
        } = this.inputAtribute

        formDiv.classList.add(inputClassForm);

        formInput.setAttribute('name', inputName)
        formInput.classList.add(inputClassFormCheck)
        formInput.setAttribute(atributeType, atributeTypeText);
        formDiv.appendChild(formInput);
        formInput.id = inputId;

        formLabel.classList.add(inputClassCheckLabel)
        formLabel.setAttribute(atributeHtmlFor, atributeHtmlForText)
        formLabel.innerHTML = inputPlaceholder;
        formDiv.appendChild(formLabel)

        return formDiv
    }

    createInput() {
        const formDiv = document.createElement('div');
        const formLabel = document.createElement('label');
        const formInput = document.createElement('input');
        const {inputName} = this.inputName;
        const {inputPlaceholder} = this.inputPlaceholder;
        const {
            inputClassForm,
            inputClassFormControl,
        } = this.inputClasses
        const {inputId} = this.inputId
        const {
            atributeType,
            atributeTypeText,
            atributeFor,
            atributeForText,
            atributeRequired,
            atributeRequiredText,
        } = this.inputAtribute

        formDiv.classList.add(inputClassForm)

        formLabel.setAttribute(atributeFor, atributeForText)

        formInput.setAttribute('name', inputName)
        formInput.placeholder = inputPlaceholder;
        formInput.classList.add(inputClassFormControl);
        formInput.id = inputId
        formInput.setAttribute(atributeType, atributeTypeText);
        formInput.setAttribute(atributeRequired, atributeRequiredText,)

        formDiv.appendChild(formLabel)

        formDiv.appendChild(formInput)

        return formDiv
    }
}

const buttonInfoSubmit = {
    buttonName: {
        buttonName: 'Подтвердить',
    },
    buttonClasses: {
        buttonClassesBtn: "btn",
        buttonClassesBtnSecondary: 'btn-primary',
    },
    buttonAtribute: {
        buttonAttribute: 'type',
        buttonAttributeText: 'submit'
    },
    buttonId: {
        buttonId: 'submit',
    }
}

const buttonInfoCancel = {
    buttonName: {
        buttonName: 'Отменить',
    },
    buttonClasses: {
        buttonClassesBtn: "btn",
        buttonClassesBtnSecondary: "btn-secondary",
    },
    buttonAtribute: {
        buttonAttribute: 'data-bs-dismiss',
        buttonAttributeText: 'modal',
    },
    buttonId: {
        buttonId: 'cancel',
    }
}

const buttonInfoCreateCard = {
    buttonName: {
        buttonName: 'Создать карточку',
    },
    buttonClasses: {
        buttonClassesBtn: "btn",
        buttonClassesBtnSecondary: 'btn-primary',
    },
    buttonAtribute: {
        buttonAttribute: 'data-toggle',
        buttonAttributeText: 'modal',
        buttonAttributeTarget: 'data-target',
        buttonAttributeTargetText: '#create-card',
        buttonAttributeType: 'type',
        buttonAttributeTypeText: 'button',
    },
    buttonId: {
        buttonId: 'card-btn',
    },
}

const buttonInfoSearch = {
    buttonName: {
        buttonName: 'search',
    },
    buttonClasses: {
        buttonClassesBtn: "btn",
        buttonClassesBtnSecondary: 'btn-primary',
        buttonClassesBtnSearch: 'btn-search',
    },
    buttonAtribute: {
        buttonAttribute: 'type',
        buttonAttributeText: 'button',
    },
    buttonId: {
        buttonId: 'search-btn',
    }
}

class Button {
    constructor({buttonName, buttonClasses, buttonAtribute, buttonId},) {
        this.buttonName = buttonName;
        this.buttonClasses = buttonClasses;
        this.buttonAtribute = buttonAtribute;
        this.buttonId = buttonId;
    }

    createButton() {
        const button = document.createElement('button');
        const {buttonName,} = this.buttonName
        const {buttonClassesBtn, buttonClassesBtnSecondary,} = this.buttonClasses
        const {
            buttonAttribute,
            buttonAttributeText,
        } = this.buttonAtribute;
        const {buttonId,} = this.buttonId

        button.classList.add(buttonClassesBtn);
        button.classList.add(buttonClassesBtnSecondary);
        button.innerHTML = buttonName;
        button.setAttribute(buttonAttribute, buttonAttributeText);
        button.id = buttonId

        return button
    }

    createButtonCreateCard() {
        const button = document.createElement('button');
        const {buttonName,} = this.buttonName
        const {buttonClassesBtn, buttonClassesBtnSecondary,} = this.buttonClasses
        const {
            buttonAttribute,
            buttonAttributeText,
            buttonAttributeTarget,
            buttonAttributeTargetText,
            buttonAttributeType,
            buttonAttributeTypeText,
        } = this.buttonAtribute;
        const {buttonId,} = this.buttonId

        button.classList.add(buttonClassesBtn);
        button.classList.add(buttonClassesBtnSecondary);
        button.innerHTML = buttonName;
        button.setAttribute(buttonAttribute, buttonAttributeText);
        button.setAttribute(buttonAttributeTarget, buttonAttributeTargetText);
        button.setAttribute(buttonAttributeType, buttonAttributeTypeText);
        button.id = buttonId

        modalElement.appendChild(button);
        return button
    }

    createButtonSearch() {
        const button = document.createElement('button');
        const {buttonName,} = this.buttonName
        const {buttonClassesBtn, buttonClassesBtnSecondary, buttonClassesBtnSearch} = this.buttonClasses
        const {
            buttonAttribute,
            buttonAttributeText,
        } = this.buttonAtribute;
        const {buttonId,} = this.buttonId

        button.classList.add(buttonClassesBtn);
        button.classList.add(buttonClassesBtnSecondary);
        button.classList.add(buttonClassesBtnSearch);
        button.innerHTML = buttonName;
        button.setAttribute(buttonAttribute, buttonAttributeText);
        button.id = buttonId;

        return button
    }
}

const textareaInfoVisit = {
    textareaName: {
        textareaName: 'description',
    },
    textAreaClasses: {
        textAreaClassFormFloating: "form-floating",
        textAreaClassFormControl: "form-control",
    },
    textAreaAttributes: {
        textAreaAttributesPlaceholder: 'placeholder',
        textAreaAttributesText: 'краткое описание визита',
        textAreaAttributesHtmlForm: 'htmlFor',
        textAreaAttributesHtmlFormText: "floatingSelect",
        atributeRequired: 'required',
        atributeRequiredText: true,
    },
    textareaId: {
        textareaId: 'textareaVisit',
    }
};

const textareaInfoIllness = {
    textareaName: {
        textareaName: 'diseasesOfTheCardiovascularSystem',
    },
    textAreaClasses: {
        textAreaClassFormFloating: "form-floating",
        textAreaClassFormControl: "form-control",
    },
    textAreaAttributes: {
        textAreaAttributesPlaceholder: 'placeholder',
        textAreaAttributesText: 'перенесенные заболевания сердечно-сосудистой системы',
        textAreaAttributesHtmlForm: 'htmlFor',
        textAreaAttributesHtmlFormText: "floatingSelect",
        atributeRequired: 'required',
        atributeRequiredText: true,
    },
    textareaId: {
        textareaId: 'textareaVisitCardio',
    }
};

class Textarea {
    constructor({textareaName, textAreaClasses, textAreaAttributes, textareaId,}) {
        this.textareaName = textareaName;
        this.textAreaClasses = textAreaClasses
        this.textAreaAttributes = textAreaAttributes
        this.textareaId = textareaId
        this.createTextArea()
    }

    createTextArea() {
        const divTextArea = document.createElement('div');
        const textArea = document.createElement('textarea');
        const textAreaLabel = document.createElement('label');
        const {textareaName,} = this.textareaName
        const {textAreaClassFormFloating, textAreaClassFormControl} = this.textAreaClasses
        const {
            textAreaAttributesPlaceholder,
            textAreaAttributesText,
            textAreaAttributesHtmlForm,
            textAreaAttributesHtmlFormText,
            atributeRequired,
            atributeRequiredText,
        } = this.textAreaAttributes
        const {textareaId,} = this.textareaId

        divTextArea.classList.add(textAreaClassFormFloating)

        textAreaLabel.setAttribute(textAreaAttributesHtmlForm, textAreaAttributesHtmlFormText);
        textAreaLabel.innerHTML = textAreaAttributesText;
        divTextArea.appendChild(textAreaLabel)

        textArea.setAttribute('name', textareaName)
        textArea.classList.add(textAreaClassFormControl)
        textArea.setAttribute(textAreaAttributesPlaceholder, textAreaAttributesText);
        textArea.setAttribute(atributeRequired, atributeRequiredText,)
        textArea.id = textareaId

        divTextArea.appendChild(textArea)

        return divTextArea
    }
}

const selectInfoDoctor = {
    selectName: {
        selectName: 'doctor',
    },
    selectClasses: {
        selectClassesFormFloating: "form-floating",
        selectClassesFormSelect: "form-select",
    },
    selectAttributes: {
        selectAttributesAria: 'aria-label',
        selectAttributesAriaText: 'doctor',
        selectAttributesSelect: 'selected',
        selectAttributesSelectText: 'true',
        selectAttributesValue1: 'value',
        selectAttributesValue1Text: 'Cardiologist',
        selectAttributesValue2Text: 'Dentist',
        selectAttributesValue3Text: 'Therapist',
        selectAttributesHtmlFor: 'htmlFor',
        selectAttributesHtmlForText: "floatingSelect"
    },
    selectedText: {
        selectedTextSelected: 'Выберите врача',
        selectedText1: 'Cardiologist',
        selectedText2: 'Dentist',
        selectedText3: 'Therapist',
    },
    selectId: {
        selectId: 'selectedDoctors',
    }
}

const selectInfoVisit = {
    selectName: {
        selectName: 'visit',
    },
    selectClasses: {
        selectClassesFormFloating: "form-floating",
        selectClassesFormSelect: "form-select",
    },
    selectAttributes: {
        selectAttributesAria: 'aria-label',
        selectAttributesAriaText: 'select-visit',
        selectAttributesSelect: 'selected',
        selectAttributesSelectText: 'true',
        selectAttributesValue1: 'value',
        selectAttributesValue1Text: 'High',
        selectAttributesValue2Text: 'Normal',
        selectAttributesValue3Text: 'Low',
        selectAttributesHtmlFor: 'htmlFor',
        selectAttributesHtmlForText: "floatingSelect"
    },
    selectedText: {
        selectedTextSelected: 'Visit',
        selectedText1: 'High',
        selectedText2: 'Normal',
        selectedText3: 'Low',
    },
    selectId: {
        selectId: 'selectedFilter',
    }
}

const selectInfoVisitSearch = {
    selectName: {
        selectName: 'visit',
    },
    selectClasses: {
        selectClassesFormFloating: "form-floating",
        selectClassesFormSelect: "form-select",
    },
    selectAttributes: {
        selectAttributesAria: 'aria-label',
        selectAttributesAriaText: 'select-visit',
        selectAttributesSelect: 'selected',
        selectAttributesSelectText: 'true',
        selectAttributesValue1: 'value',
        selectAttributesValue1Text: 'High',
        selectAttributesValue2Text: 'Normal',
        selectAttributesValue3Text: 'Low',
        selectAttributesHtmlFor: 'htmlFor',
        selectAttributesHtmlForText: "floatingSelect"
    },
    selectedText: {
        selectedTextSelected: 'Visit',
        selectedText1: 'High',
        selectedText2: 'Normal',
        selectedText3: 'Low',
    },
    selectId: {
        selectId: 'selectedFilterSearch',
    }
}


const handleChange = (value) => {
    formCreatorCards.removeElements()
    if (value === 'Cardiologist') {
        const cardiolog = [
            inputFullName.createInput(),
            inputAge.createInput(),
            inputPurpose.createInput(),
            inputIndexBody.createInput(),
            inputNormalPressure.createInput(),
            textareaIll.createTextArea(),
            textareaVisits.createTextArea(),
            selectVisits.createSelectSearch(),]
        formCreatorCards.createInputs(cardiolog)
    } else if (value === 'Dentist') {
        const dentist = [
            inputFullName.createInput(),
            inputLastDateVisit.createInput(),
            inputPurpose.createInput(),
            textareaVisits.createTextArea(),
            selectVisits.createSelectSearch(),]
        formCreatorCards.createInputs(dentist)
    } else if (value === 'Therapist') {
        const therapist = [
            inputFullName.createInput(),
            inputAge.createInput(),
            inputPurpose.createInput(),
            textareaVisits.createTextArea(),
            selectVisits.createSelectSearch(),
        ]
        formCreatorCards.createInputs(therapist)
    }
}

class Select {
    constructor({selectName, selectClasses, selectAttributes, selectedText, selectId,}, {onChange}) {
        this.selectName = selectName
        this.selectClasses = selectClasses
        this.selectAttributes = selectAttributes
        this.selectedText = selectedText;
        this.selectId = selectId;
        this.divSelect = document.createElement('div');
        this.select = document.createElement('select');
        this.onChange = onChange;
    }

    createSelect() {
        const optionSelected = document.createElement('option');
        const optionValue1 = document.createElement('option');
        const optionValue2 = document.createElement('option');
        const optionValue3 = document.createElement('option');
        const selectLabel = document.createElement('label');
        const {selectName,} = this.selectName;
        const {selectClassesFormFloating, selectClassesFormSelect,} = this.selectClasses
        const {
            selectAttributesAria,
            selectAttributesAriaText,
            selectAttributesSelect,
            selectAttributesSelectText,
            selectAttributesValue1,
            selectAttributesValue1Text,
            selectAttributesValue2Text,
            selectAttributesValue3Text,
            selectAttributesHtmlFor,
            selectAttributesHtmlForText,
        } = this.selectAttributes
        const {
            selectedTextSelected,
            selectedText1,
            selectedText2,
            selectedText3,
        } = this.selectedText
        const {selectId,} = this.selectId

        this.divSelect.classList.add(selectClassesFormFloating)

        selectLabel.innerHTML = selectedTextSelected;
        selectLabel.setAttribute(selectAttributesHtmlFor, selectAttributesHtmlForText);
        this.divSelect.appendChild(selectLabel)

        this.select.classList.add(selectClassesFormSelect)
        this.select.setAttribute('name', selectName);
        this.select.id = selectId
        this.select.setAttribute(selectAttributesAria, selectAttributesAriaText)
        this.divSelect.appendChild(this.select)

        // for (let i = 0; i < this.selectedTextDoctors.length; i++) {
        //     const option = document.createElement('option');
        //     for (let j = 0; j < this.selectAttributes.length; j++) {
        //         option.setAttribute('selected', 'selected')
        //         optionSelected.innerHTML = this.doctors
        //         select.appendChild(optionSelected)
        //     }
        // } ???????????

        optionSelected.setAttribute(selectAttributesSelect, selectAttributesSelectText)
        optionSelected.innerHTML = selectName
        this.select.appendChild(optionSelected)

        optionValue1.setAttribute(selectAttributesValue1, selectAttributesValue1Text)
        optionValue1.innerHTML = selectedText1;
        this.select.appendChild(optionValue1)

        optionValue2.setAttribute(selectAttributesValue1, selectAttributesValue2Text)
        optionValue2.innerHTML = selectedText2;
        this.select.appendChild(optionValue2)

        optionValue3.setAttribute(selectAttributesValue1, selectAttributesValue3Text)
        optionValue3.innerHTML = selectedText3;
        this.select.appendChild(optionValue3);

        this.select.addEventListener('change', this.handleChangeSelect.bind(this))
        return this.divSelect
    }

    createSelectSearch() {
        const optionSelected = document.createElement('option');
        const optionValue1 = document.createElement('option');
        const optionValue2 = document.createElement('option');
        const optionValue3 = document.createElement('option');
        const selectLabel = document.createElement('label');
        const {selectName,} = this.selectName;
        const {selectClassesFormFloating, selectClassesFormSelect,} = this.selectClasses
        const {
            selectAttributesAria,
            selectAttributesAriaText,
            selectAttributesSelect,
            selectAttributesSelectText,
            selectAttributesValue1,
            selectAttributesValue1Text,
            selectAttributesValue2Text,
            selectAttributesValue3Text,
            selectAttributesHtmlFor,
            selectAttributesHtmlForText,
        } = this.selectAttributes
        const {
            selectedTextSelected,
            selectedText1,
            selectedText2,
            selectedText3,
        } = this.selectedText
        const {selectId,} = this.selectId

        this.divSelect.classList.add(selectClassesFormFloating)

        selectLabel.innerHTML = selectedTextSelected;
        selectLabel.setAttribute(selectAttributesHtmlFor, selectAttributesHtmlForText);
        this.divSelect.appendChild(selectLabel)

        this.select.classList.add(selectClassesFormSelect)
        this.select.setAttribute('name', selectName);
        this.select.id = selectId
        this.select.setAttribute(selectAttributesAria, selectAttributesAriaText)
        this.divSelect.appendChild(this.select)

        // for (let i = 0; i < this.selectedTextDoctors.length; i++) {
        //     const option = document.createElement('option');
        //     for (let j = 0; j < this.selectAttributes.length; j++) {
        //         option.setAttribute('selected', 'selected')
        //         optionSelected.innerHTML = this.doctors
        //         select.appendChild(optionSelected)
        //     }
        // } ???????????

        optionSelected.setAttribute(selectAttributesSelect, selectAttributesSelectText)
        optionSelected.innerHTML = selectName
        this.select.appendChild(optionSelected)

        optionValue1.setAttribute(selectAttributesValue1, selectAttributesValue1Text)
        optionValue1.innerHTML = selectedText1;
        this.select.appendChild(optionValue1)

        optionValue2.setAttribute(selectAttributesValue1, selectAttributesValue2Text)
        optionValue2.innerHTML = selectedText2;
        this.select.appendChild(optionValue2)

        optionValue3.setAttribute(selectAttributesValue1, selectAttributesValue3Text)
        optionValue3.innerHTML = selectedText3;
        this.select.appendChild(optionValue3);

        return this.divSelect
    }

    handleChangeSelect(e) {
        this.onChange(this.select.value)
    }
}

const selectDoctors = new Select(selectInfoDoctor, {onChange: handleChange})
const selectVisits = new Select(selectInfoVisit, {onChange: handleChange})
const selectVisitSearch = new Select(selectInfoVisitSearch, {onChange: handleChange})
const selectEditCardDoctor = new Select(selectInfoDoctor, {onChange: handleChange});
const selectEditCardVisit = new Select(selectInfoVisit, {onChange: handleChange})

const textareaIll = new Textarea(textareaInfoIllness);
const textareaVisits = new Textarea(textareaInfoVisit)

const inputEmail = new Input(inputInfoMail);
const inputPass = new Input(inputInfoPass);
const inputSearch = new Input(inputInfoSearch);
const inputCheckOpen = new Input(inputInfoCheckOpen);
const inputCheckDone = new Input(inputInfoCheckDone);
const inputFullName = new Input(inputInfoFullName);
const inputAge = new Input(inputInfoAge);
const inputPurpose = new Input(inputInfoPurposeVisit);
const inputIndexBody = new Input(inputInfoIndexBody);
const inputLastDateVisit = new Input(inputInfoLastDate);
const inputNormalPressure = new Input(inputInfoNormalPressure);
const inputRememberMe = new Input(inputInfoRememberMe);

const buttonCancel = new Button(buttonInfoCancel)
const buttonSubmit = new Button(buttonInfoSubmit)
const buttonCreateCard = new Button(buttonInfoCreateCard);
const buttonSearch = new Button(buttonInfoSearch);

const searchInfo = {
    searchName: {
        searchNameForm: 'Search'
    },
    searchClasses: {
        searchClassRow: 'row',
        searchClassCol3: 'col-3',
        searchClassCol1: 'col-1',
        searchClassFormCheck: 'form-check',
        searchClassFormCheckInline: 'form-check-inline',
    },
    searchParent: {
        searchParentCont: searchParentContainer,
    },
    searchChildren: {
        searchChildInput: inputSearch.createInputSearch(),
        searchChildButton: buttonSearch.createButtonSearch(),
        searchChildInputCheckBox1: inputCheckOpen.createInputCheck(),
        searchChildInputCheckBox2: inputCheckDone.createInputCheck(),
        searchSelect: selectVisitSearch.createSelectSearch(),
    },
}

const renderCards = (cards) => {
    const cardContainer = document.querySelector('#cards')
    cardContainer.innerHTML = ''
    if (cards.length === 0) {
        const noCard = document.createElement('div');
        noCard.classList.add('col-3')
        noCard.innerHTML = `No items have been added`;
        parentCard.appendChild(noCard)
    } else {
        for (let card of cards) {
            if (card.doctor === 'Cardiologist') {
                const visitCardiologist = new VisitsCardiologist(parentCard, cardInfo, card, {
                    onEdit: cardEdit,
                    onDelete: cardDelete
                })
                visitCardiologist.createCardCardiologist()
            } else if (card.doctor === 'Dentist') {
                const visitDentist = new VisitsDentist(parentCard, cardInfo, card, {
                    onEdit: cardEdit,
                    onDelete: cardDelete
                })
                visitDentist.createCardDentist()
            } else if (card.doctor === 'Therapist') {
                const visitTherapist = new VisitsTherapist(parentCard, cardInfo, card, {
                    onEdit: cardEdit,
                    onDelete: cardDelete,
                })
                visitTherapist.createCardTherapist(cards)
            }
        }
    }
}

const search = (searchValue) => {
    api.getRequestAll()
        .then((response) => {
            const filtered = response.filter((card) =>{
                console.log(card)
               return card.title.includes(searchValue) || card.description.includes(searchValue)
            })

        renderCards(filtered)
        })

    }

class SearchCreatorForm {
    constructor({searchName, searchClasses, searchParent, searchChildren}, {onSearch}) {
        this.searchName = searchName;
        this.searchClasses = searchClasses;
        this.searchParent = searchParent;
        this.searchChildren = searchChildren;
        this.onSearch = onSearch;
        this.searchInput = ''
    }

    createSearchForm() {
        const searchRow = document.createElement('div')
        const searchCol3First = document.createElement('div')
        const searchCol3Second = document.createElement('div')
        const searchCol3Third = document.createElement('div')
        const searchCol1First = document.createElement('div')
        const searchCol1Second = document.createElement('div')
        const {
            searchClassRow,
            searchClassCol3,
            searchClassCol1,
        } = this.searchClasses;
        const {searchParentCont} = this.searchParent;
        const {
            searchChildInput,
            searchChildButton,
            searchChildInputCheckBox1,
            searchChildInputCheckBox2,
            searchSelect
        } = this.searchChildren;

        searchRow.classList.add(searchClassRow);
        searchParentCont.appendChild(searchRow)

        searchCol3First.classList.add(searchClassCol3);
        searchRow.appendChild(searchCol3First);

        searchCol3First.appendChild(searchChildInput);

        searchCol3Second.classList.add(searchClassCol3);
        searchRow.appendChild(searchCol3Second);

        searchCol3Second.appendChild(searchChildButton)

        searchCol1First.classList.add(searchClassCol1);
        searchRow.appendChild(searchCol1First);

        searchCol1First.appendChild(searchChildInputCheckBox1);

        searchCol1Second.classList.add(searchClassCol1);
        searchRow.appendChild(searchCol1Second);

        searchCol1Second.appendChild(searchChildInputCheckBox2);

        searchCol3Third.classList.add(searchClassCol3);
        searchRow.appendChild(searchCol3Third);

        searchCol3Third.appendChild(searchSelect)

        searchChildButton.addEventListener('click', this.filter.bind(this))
        searchChildInput.addEventListener('keyup', this.updateSearchInput.bind(this));
    }

    updateSearchInput (e){
        this.searchInput = e.target.value.toLowerCase();
        console.log(e)
        // this.filter()
    }

    filter (e){
        console.log(this.searchInput)
        this.onSearch(this.searchInput)
        // const filtered = card.filter(item => {
        //     if (this.searchInput.length > 2 &&
        //         !item.doctor.toLowerCase().includes(this.searchInput) &&
        //         !item.fullName.toLowerCase().includes(this.searchInput)) {
        //         return false
        //     }
        //     if (sizesChecked.size > 0 && !sizesChecked.has(item.size)) {
        //         return false;
        //     }
        //     return true;
        // });
        // render(filtered);
    }
}

const searchForm = new SearchCreatorForm(searchInfo, {onSearch: search})

const cardEdit = (card) => {
    console.log(card, '1')
    modalCardEdit.createModalWindow(card)
}

const cardDelete = (id) => {
    api.deleteRequest(id)
}

const handleSuccessfulLogin = () => {
    hideModal();
    searchForm.createSearchForm();
    buttonEnter.remove();
    buttonCreateCard.createButtonCreateCard();
    api.getUrlRequest().then((cards) => {
        renderCards(cards)
    })
}

const handleLogin = (data) => {
    api.auth(data).then(() => {
        handleSuccessfulLogin()
    }).catch((error) => {
        alert(error)
    })
};

document.addEventListener('DOMContentLoaded', () => {
    const token = localStorage.getItem('token')
    if (token) {
        api.setToken(token)
        handleSuccessfulLogin()
    }
})

const handleDoctor = (card) => {
    hideModalDoctor();
    api.postRequest(card).then(() => {
        const visitCard = new VisitsCardiologist(parentCard, cardInfo, card, {onEdit: cardEdit, onDelete: cardDelete})
        visitCard.createCardCardiologist(card)
    })
}

const loginConfig = [
    {
        name: "name",
        type: "email",
        element: 'input'
    },
    {
        name: "password",
        type: "password",
        element: 'input'

    },
    {
        name: "comment",
        element: 'textarea'

    },
];

const doctorConfig = [
    {
        name: "name",
        type: "email",
    },
    {
        name: "password",
        type: "password",
    },
];

const formInfo = {
    formName: {
        formEnter: 'Registration Form',
    },
    formEnterChildren: [
        inputEmail.createInputMail(),
        inputPass.createInputPass(),
        inputRememberMe.createInputCheckRemember(),
    ],
    formEnterChildrenButton: [
        buttonCancel.createButton(),
        buttonSubmit.createButton(),
    ],
    formId: {
        formId: 'enter-form'
    }
}

const formInfoCards = {
    formName: {
        formEnter: 'Create Card',
    },
    formEnterChildren: [

        selectDoctors.createSelect()
    ],
    formEnterChildrenButton: [
        buttonCancel.createButton(),
        buttonSubmit.createButton(),
    ],
    formId: {
        formId: 'form-card'
    }
}

const formInfoCardsEdit = {
    formName: {
        formEnter: 'Edit Card',
    },
    formEnterChildren: [
        selectEditCardDoctor.createSelect(),
        inputFullName.createInput(),
        inputAge.createInput(),
        inputPurpose.createInput(),
        inputIndexBody.createInput(),
        inputNormalPressure.createInput(),
        textareaIll.createTextArea(),
        textareaVisits.createTextArea(),
        selectEditCardVisit.createSelectSearch(),
    ],
    formEnterChildrenButton: [
        buttonCancel.createButton(),
        buttonSubmit.createButton(),
    ],
    formId: {
        formId: 'form-card-edit'
    }
}

class FormCreator {
    static FieldsDictionary = {
        input: Input,
        select: Select,
        textarea: Textarea,
    };

    constructor({formName, formEnterChildren, formEnterChildrenButton, formId}, {config, onSubmit}) {
        this.config = config;
        this._onSubmit = onSubmit;
        this.formName = formName;
        this.formId = formId
        this.formEnterChildren = formEnterChildren;
        this.formEnterChildrenButton = formEnterChildrenButton;
        this.form = document.createElement('form');
        this.divForm = document.createElement('div');
        this.formDiv = document.createElement('div');
        // this.createForm();
    }

    createDivChildren(card) {
        if (this.formName.formEnter === 'Registration Form') {
            const formRegistration = this.createForm()
            this.form.appendChild(formRegistration)
            this.form.addEventListener('submit', this.handleSubmit.bind(this))
            return this.form
        } else if (this.formName.formEnter === 'Edit Card') {
            const formEdit = this.createInputsEdit(card)
            this.form.appendChild(formEdit)
            this.form.addEventListener('submit', this.handleSubmitEdit.bind(this))
            return this.form
        } else if (this.formName.formEnter === 'Create Card') {
            const selectDiv = document.createElement('div');
            this.form.appendChild(this.divForm)
            this.formDiv.classList.add('form')
            this.formDiv.id = 'cont'
            this.divForm.appendChild(this.formDiv)

            const [select] = this.formEnterChildren
            this.form.appendChild(selectDiv)
            selectDiv.appendChild(select)
            this.formEnterChildrenButton.forEach(item => this.form.appendChild(item))
            this.form.addEventListener('submit', this.handleSubmitCard.bind(this))
            return this.form
        }
        return this.form
    }

    createForm() {
        const {formId} = this.formId
        this.form.id = formId
        this.formEnterChildren.forEach(item => this.divForm.appendChild(item));
        this.formEnterChildrenButton.forEach(item => this.divForm.appendChild(item));

        return this.divForm
    }

    createInputs(doctor) {
        // const form = document.querySelector('#form-card')
        doctor.forEach(input => this.formDiv.appendChild(input))
    }

    createInputsEdit(card) {
        console.log(card)
        const {formId} = this.formId
        this.form.id = formId
        this.formEnterChildren.forEach(item => this.divForm.appendChild(item));
        this.formEnterChildrenButton.forEach(item => this.divForm.appendChild(item));

        return this.divForm
    }

    handleSubmitEdit(e) {
        e.preventDefault()
        const inputs = this.form.querySelectorAll('input');
        const textareas = this.form.querySelectorAll('textarea');
        const selects = this.form.querySelectorAll('select');
        const formData = {}
        inputs.forEach(input => {
            formData[input.name] = input.value;
        })
        textareas.forEach(textarea => {
            formData[textarea.name] = textarea.value;
        })
        selects.forEach(select => {
            console.log(formData[select.name])
            console.log(select.value)
            formData[select.name] = select.value;
        })
        this._onSubmit(formData)
        console.log(e)
    }

    handleSubmit(e) {
        e.preventDefault()
        const inputs = this.form.querySelectorAll('input');
        const formData = {}
        inputs.forEach(input => {
            formData[input.name] = input.value;
        })
        this._onSubmit(formData)
    }

    handleSubmitCard(e) {
        e.preventDefault()
        const inputs = this.form.querySelectorAll('input');
        const textareas = this.form.querySelectorAll('textarea');
        const selects = this.form.querySelectorAll('select');
        const formData = {}
        inputs.forEach(input => {
            formData[input.name] = input.value;
        })
        textareas.forEach(textarea => {
            formData[textarea.name] = textarea.value;
        })
        selects.forEach(select => {
            formData[select.name] = select.value;
        })
        this._onSubmit(formData)
    }

    removeElements() {
        const removeDiv = document.querySelector('#cont')
        if (removeDiv) {
            removeDiv.innerHTML = ''
        }
    }
}

const doctorEdit = () => {

}

const editDoctor = (id) => {
    api.putRequest(id)
}

const formCreator = new FormCreator(formInfo, {config: loginConfig, onSubmit: handleLogin});
const formCreatorCards = new FormCreator(formInfoCards, {doctor: doctorConfig, onSubmit: handleDoctor});
const formEditCards = new FormCreator(formInfoCardsEdit, {editDoctor: doctorEdit, onSubmit: editDoctor});

const modalInfoCard = {
    modalName: {
        headerModalName: 'Создать карточку',
    },
    modalClasses: {
        modalModal: 'modal',
        modalDialog: 'modal-dialog',
        modalContent: 'modal-content',
        modalHeader: 'modal-header',
        modalTitle: 'modal-title',
        modalClose: 'close',
        modalBody: 'modal-body',
        modalFooter: 'modal-footer',
    },
    modalId: {
        modalId: 'create-card',
        modalHeaderId: 'modalCards',
    },
    modalChildren: {
        modalForm: formCreatorCards.createDivChildren()
    },
    modalAtribute: {
        modalTabIndex: 'tabIndex',
        modalTabIndexText: -1,
        modalAtributeType: 'type',
        modalButtonType: 'button',
        modalAtributeDismiss: 'data-dismiss',
        modalTextDismiss: 'modal',
        modalAtributeLabel: 'aria-label',
        modalTextLabel: 'Close',
        modalAriaHidden: 'aria-hidden',
        modalAriaHiddenText: "true",
    },
    modalParent: {
        modalParent: parentCard,
    }
}

const modalInfoCardEdit = {
    modalName: {
        headerModalName: 'редактировать карточку',
    },
    modalClasses: {
        modalModal: 'modal',
        modalDialog: 'modal-dialog',
        modalContent: 'modal-content',
        modalHeader: 'modal-header',
        modalTitle: 'modal-title',
        modalClose: 'close',
        modalBody: 'modal-body',
        modalFooter: 'modal-footer',
    },
    modalId: {
        modalId: 'edit-card',
        modalHeaderId: 'modalEditCards',
    },
    modalChildren: {
        modalForm: formEditCards.createDivChildren()
    },
    modalAtribute: {
        modalTabIndex: 'tabIndex',
        modalTabIndexText: -1,
        modalAtributeType: 'type',
        modalButtonType: 'button',
        modalAtributeDismiss: 'data-dismiss',
        modalTextDismiss: 'modal',
        modalAtributeLabel: 'aria-label',
        modalTextLabel: 'Close',
        modalAriaHidden: 'aria-hidden',
        modalAriaHiddenText: "true",
    },
    modalParent: {
        modalParent: editContainer,
    }
}

const modalInfo = {
    modalName: {
        headerModalName: 'Регистрационная форма',
    },
    modalClasses: {
        modalModal: 'modal',
        modalDialog: 'modal-dialog',
        modalContent: 'modal-content',
        modalHeader: 'modal-header',
        modalTitle: 'modal-title',
        modalClose: 'close',
        modalBody: 'modal-body',
        modalFooter: 'modal-footer',
    },
    modalId: {
        modalId: 'enter',
        modalHeaderId: 'modalEnter',
    },
    modalChildren: {
        modalForm: formCreator.createDivChildren()
    },
    modalAtribute: {
        modalTabIndex: 'tabIndex',
        modalTabIndexText: -1,
        modalAtributeType: 'type',
        modalButtonType: 'button',
        modalAtributeDismiss: 'data-dismiss',
        modalTextDismiss: 'modal',
        modalAtributeLabel: 'aria-label',
        modalTextLabel: 'Close',
        modalAriaHidden: 'aria-hidden',
        modalAriaHiddenText: "true",
    },
    modalParent: {
        modalParent: modalElement,
    }
}

class Modal {
    constructor({modalName, modalClasses, modalId, modalChildren, modalAtribute, modalParent}) {
        this.modalName = modalName;
        this.modalParent = modalParent;
        this.modalChildren = modalChildren;
        this.modalClasses = modalClasses;
        this.modalAtribute = modalAtribute;
        this.modalId = modalId;
        this.modalWindow = document.createElement('div');
        this.createModalWindow();
    }

    createModalWindow(card) {
        const modalDialogElem = document.createElement('div');
        const modalContentElem = document.createElement('div');
        const modalHeaderElem = document.createElement('div');
        const header = document.createElement('h5');
        const bt = document.createElement('button');
        const modalBodyElem = document.createElement('div');
        // const modalFooterElem = document.createElement('div');
        const modalSpan = document.createElement('span')
        const {headerModalName} = this.modalName
        const {
            modalModal,
            modalDialog,
            modalContent,
            modalHeader,
            modalTitle,
            modalClose,
            modalBody,
            // modalFooter
        } = this.modalClasses;
        const {
            modalTabIndex,
            modalTabIndexText,
            modalAtributeType,
            modalButtonType,
            modalAtributeDismiss,
            modalTextDismiss,
            modalAtributeLabel,
            modalTextLabel,
            modalAriaHidden,
            modalAriaHiddenText
        } = this.modalAtribute;
        const {modalId, modalHeaderId} = this.modalId;
        const {modalParent} = this.modalParent;
        const {modalForm} = this.modalChildren;

        this.modalWindow.classList.add(modalModal);
        this.modalWindow.id = modalId;
        this.modalWindow.setAttribute(modalTabIndex, modalTabIndexText);
        document.body.appendChild(this.modalWindow)

        modalDialogElem.classList.add(modalDialog);
        this.modalWindow.appendChild(modalDialogElem)


        modalContentElem.classList.add(modalContent);
        modalDialogElem.appendChild(modalContentElem);

        modalHeaderElem.classList.add(modalHeader);
        modalContentElem.appendChild(modalHeaderElem);

        header.classList.add(modalTitle);
        header.id = modalHeaderId;
        header.innerHTML = headerModalName;

        modalHeaderElem.appendChild(header)

        bt.classList.add(modalClose);
        bt.setAttribute(modalAtributeType, modalButtonType,)
        bt.setAttribute(modalAtributeDismiss, modalTextDismiss,);
        bt.setAttribute(modalAtributeLabel, modalTextLabel)
        modalHeaderElem.appendChild(bt);

        modalSpan.setAttribute(modalAriaHidden, modalAriaHiddenText);
        modalSpan.innerHTML = '&times;'
        bt.appendChild(modalSpan)

        modalBodyElem.classList.add(modalBody);
        modalContentElem.appendChild(modalBodyElem);

        modalBodyElem.appendChild(modalForm)

        return modalForm
    }
}

const modalEnter = new Modal(modalInfo);
const modalCard = new Modal(modalInfoCard);
const modalCardEdit = new Modal(modalInfoCardEdit)

class API {
    constructor({baseUrl, baseUrlLogin, baseUrlCards},) {
        this.baseUrl = baseUrl;
        this.baseUrlLogin = baseUrlLogin;
        this.baseUrlCards = baseUrlCards;
        this.token = null;
        this.id = null;
        this.getTokenLocalStorage();
    }

    getTokenLocalStorage() {
        const token = localStorage.getItem('token');
        if (token) {
            this.token = token;
        }
        return token
    }

    async auth(data) {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}${this.baseUrlLogin}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        console.log(response.ok)
        if(response.ok){
            console.log(response)
            const token = await response.text()
            this.token = token;
            localStorage.setItem('token', this.token)
        } else{
            return Promise.reject('you are not authorized')
        }
    }

    setToken(token){
        this.token = token;
    }

    async getUrlRequest() {
        if (!this.token) {
            throw new Error('you are not authorized')
        } else {
            const response = await fetch(`${this.baseUrl}${this.baseUrlCards}`, {
                method: 'Get',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${this.token}`
                },
            })
            const responseData = await response.json()
            return responseData
        }
    }

    async postRequest(body) {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
            body: JSON.stringify(body)
        })
        const responseData = await response.json()
        return responseData
    }

    async getRequestAll() {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}`, {
            method: 'Get',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
        })
        const responseData = await response.json()
        return responseData
    }

    async getRequestOne() {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}/${this.id}`, {
            method: 'Get',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
        })
        const responseData = await response.json()
        return responseData
    }

    async putRequest(id) {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}/${this.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
            body: JSON.stringify(this.patientInfo)
        })
        const responseData = await response.json()
        return responseData
    }

    async deleteRequest(id) {
        const response = await fetch(`${this.baseUrl}${this.baseUrlCards}/${id}`, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${this.token}`
            },
        })
        const responseData = await response.json()
        return responseData
    }
}

const api = new API(baseUrls,)

function hideModal() {
    $('#enter').modal('hide')
}

function hideModalDoctor() {
    $('#create-card').modal('hide')
}

const cardInfo = {
    cardClasses: {
        cardClassCol: 'col-3',
        cardClassButtonDisplay: 'cardButtonDisplay',
        cardClassButtonAll: "btn",
        cardClassButtonMore: 'btn-info',
        cardClassDelete: 'btn-warning',
        cardClassEdit: 'btn-primary',
    },
    buttonAtribute: {
        buttonAttribute: 'type',
        buttonAttributeText: 'button',
    },
}

class Visits {
    constructor(parentCard, {cardClasses, buttonAtribute}, card, {onEdit: cardEdit, onDelete: cardDelete}) {
        this.parentCard = parentCard;
        this.cardClasses = cardClasses;
        this.card = card;
        this.onEdit = cardEdit
        this.onDelete = cardDelete
        this.buttonAtribute = buttonAtribute
    }
}

class VisitsCardiologist extends Visits {
    constructor(parentCard, {cardClasses, buttonAtribute}, card, {onEdit: cardEdit, onDelete: cardDelete}) {
        super(parentCard, {cardClasses, buttonAtribute}, card, {onEdit: cardEdit, onDelete: cardDelete})
        this.parentCard = parentCard
        this.card = card;
        this.onEdit = cardEdit
        this.onDelete = cardDelete
        // this.buttonAtribute = buttonAtribute
        this.cardDivText = document.createElement('div')
        this.cardDiv = document.createElement('div');
    }

    createCardCardiologist() {
        const {
            patientFullName,
            id,
            doctor,
        } = this.card
        const cardButtonMore = document.createElement('button')
        const cardEdit = document.createElement('button');
        const cardDelete = document.createElement('button')
        const cardPatientFullName = document.createElement('p');
        const cardDoctor = document.createElement('p');
        const cardId = document.createElement('p');
        const cardButtonDiv = document.createElement('div')
        const {
            cardClassCol, cardClassButtonAll, cardClassButtonMore, cardClassDelete,
            cardClassEdit, cardClassButtonDisplay
        } = this.cardClasses;
        // const {buttonAttribute, buttonAttributeText} = this.buttonAtribute;

        this.cardDiv.classList.add(cardClassCol);
        this.cardDiv.id = id;
        this.parentCard.appendChild(this.cardDiv);

        this.cardDiv.appendChild(this.cardDivText)

        cardPatientFullName.innerHTML = `Фио ${patientFullName}`;
        this.cardDivText.appendChild(cardPatientFullName);

        cardDoctor.innerHTML = `Доктор ${doctor}`;
        this.cardDivText.appendChild(cardDoctor);

        cardButtonDiv.classList.add(cardClassButtonDisplay);
        this.cardDiv.appendChild(cardButtonDiv);

        cardButtonMore.innerHTML = 'Показать больше информации'
        cardButtonMore.classList.add(cardClassButtonAll);
        cardButtonMore.classList.add(cardClassButtonMore)
        // cardButtonMore.setAttribute(buttonAttribute, buttonAttributeText);
        cardButtonDiv.appendChild(cardButtonMore);

        cardEdit.innerHTML = 'Редактировать карточку'
        cardEdit.classList.add(cardClassButtonAll)
        cardEdit.classList.add(cardClassEdit)
        cardEdit.setAttribute('data-toggle', "modal")
        cardEdit.setAttribute('data-target', "#edit-card")
        // cardEdit.setAttribute(buttonAttribute, buttonAttributeText);
        cardButtonDiv.appendChild(cardEdit);

        cardDelete.innerHTML = 'Удалить карточку'
        cardDelete.classList.add(cardClassButtonAll)
        cardDelete.classList.add(cardClassDelete)
        // cardDelete.setAttribute(buttonAttribute, buttonAttributeText);
        cardButtonDiv.appendChild(cardDelete);

        cardButtonMore.addEventListener('click', this.showMoreCardiologist.bind(this))
        cardEdit.addEventListener('click', this.editCard.bind(this))
        cardDelete.addEventListener('click', this.deleteCard.bind(this))

        return this.cardDiv
    }

    showMoreCardiologist(e) {
        const {
            normalPressure,
            bodyMassIndex,
            diseasesOfTheCardiovascularSystem,
            age,
            title,
            description,
            visit,
        } = this.card
        const cardNormalPressure = document.createElement('p');
        const cardBodyMassIndex = document.createElement('p');
        const cardDiseasesOfTheCardiovascularSystem = document.createElement('p');
        const cardTitle = document.createElement('p');
        const cardDescription = document.createElement('p');
        const cardVisit = document.createElement('p');
        const cardAge = document.createElement('p')

        cardNormalPressure.innerHTML = `Обычное давление ${normalPressure}`;
        this.cardDivText.appendChild(cardNormalPressure)

        cardTitle.innerHTML = `Цель визита ${title}`;
        this.cardDivText.appendChild(cardTitle)

        cardDescription.innerHTML = `Краткое описание ${description}`;
        this.cardDivText.appendChild(cardDescription)

        cardBodyMassIndex.innerHTML = `Индекс массы тела ${bodyMassIndex}`;
        this.cardDivText.appendChild(cardBodyMassIndex)

        cardVisit.innerHTML = `Срочность визита ${visit}`;
        this.cardDivText.appendChild(cardVisit)

        cardDiseasesOfTheCardiovascularSystem.innerHTML = `Перенесенные заболевания сердечно сосудистой системы ${diseasesOfTheCardiovascularSystem}`;
        this.cardDivText.appendChild(cardDiseasesOfTheCardiovascularSystem);

        cardAge.innerHTML = `Возвраст ${age}`;
        this.cardDivText.appendChild(cardAge);
    }

    editCard(e) {
        this.onEdit(this.card)
    }

    deleteCard(e) {
        const {id} = this.card
        this.cardDiv.remove()

        this.onDelete(id)
    }
}


class VisitsDentist extends Visits {
    constructor(parentCard, {cardClasses, buttonAtribute}, card, {onEdit: cardEdit, onDelete: cardDelete}) {
        super(parentCard, {cardClasses, buttonAtribute}, card, {onEdit: cardEdit, onDelete: cardDelete})
        this.parentCard = parentCard
        this.card = card;
        this.onEdit = cardEdit
        this.onDelete = cardDelete
        // this.buttonAtribute = buttonAtribute
        this.cardDivText = document.createElement('div')
        this.cardDiv = document.createElement('div');
    }

    createCardDentist() {
        const {
            patientFullName,
            id,
            doctor,
        } = this.card
        const cardButtonMore = document.createElement('button')
        const cardEdit = document.createElement('button');
        const cardDelete = document.createElement('button')
        const cardPatientFullName = document.createElement('p');
        const cardDoctor = document.createElement('p');
        const cardId = document.createElement('p');
        const cardButtonDiv = document.createElement('div')
        const {
            cardClassCol, cardClassButtonAll, cardClassButtonMore, cardClassDelete,
            cardClassEdit, cardClassButtonDisplay
        } = this.cardClasses;
        // const {buttonAttribute, buttonAttributeText} = this.buttonAtribute;

        this.cardDiv.classList.add(cardClassCol);
        this.cardDiv.id = id;
        this.parentCard.appendChild(this.cardDiv);

        this.cardDiv.appendChild(this.cardDivText)

        cardPatientFullName.innerHTML = `Фио ${patientFullName}`;
        this.cardDivText.appendChild(cardPatientFullName);

        cardDoctor.innerHTML = `Доктор ${doctor}`;
        this.cardDivText.appendChild(cardDoctor);

        cardButtonDiv.classList.add(cardClassButtonDisplay);
        this.cardDiv.appendChild(cardButtonDiv);

        cardButtonMore.innerHTML = 'Показать больше информации'
        cardButtonMore.classList.add(cardClassButtonAll);
        cardButtonMore.classList.add(cardClassButtonMore)
        // cardButtonMore.setAttribute(buttonAttribute, buttonAttributeText);
        cardButtonDiv.appendChild(cardButtonMore);

        cardEdit.innerHTML = 'Редактировать карточку'
        cardEdit.classList.add(cardClassButtonAll)
        cardEdit.classList.add(cardClassEdit)
        // cardEdit.setAttribute(buttonAttribute, buttonAttributeText);
        cardEdit.setAttribute('data-toggle', "modal")
        cardEdit.setAttribute('data-target', "#edit-card")
        cardButtonDiv.appendChild(cardEdit);

        cardDelete.innerHTML = 'Удалить карточку'
        cardDelete.classList.add(cardClassButtonAll)
        cardDelete.classList.add(cardClassDelete)
        // cardDelete.setAttribute(buttonAttribute, buttonAttributeText);
        cardButtonDiv.appendChild(cardDelete);

        cardButtonMore.addEventListener('click', this.showMoreDentist.bind(this))
        cardEdit.addEventListener('click', this.editCard.bind(this))
        cardDelete.addEventListener('click', this.deleteCard.bind(this))

        return this.cardDiv
    }

    showMoreDentist(e) {
        const {
            title,
            description,
            visit,
            dateLastVisit,
        } = this.card
        const cardLastDateVisit = document.createElement('p');
        const cardTitle = document.createElement('p');
        const cardDescription = document.createElement('p');
        const cardVisit = document.createElement('p');

        cardLastDateVisit.innerHTML = `Последний визит ${dateLastVisit}`;
        this.cardDivText.appendChild(cardLastDateVisit)

        cardTitle.innerHTML = `Цель визита ${title}`;
        this.cardDivText.appendChild(cardTitle)

        cardDescription.innerHTML = `Краткое описание ${description}`;
        this.cardDivText.appendChild(cardDescription)

        cardVisit.innerHTML = `Срочность визита ${visit}`;
        this.cardDivText.appendChild(cardVisit)
    }

    editCard(e) {
        this.onEdit(this.card)
    }

    deleteCard(e) {
        const {id} = this.card
        this.cardDiv.remove()

        this.onDelete(id)
    }
}


class VisitsTherapist extends Visits {
    constructor(parentCard, {cardClasses, buttonAtribute}, card, {onEdit: cardEdit, onDelete: cardDelete}) {
        super(parentCard, {cardClasses, buttonAtribute}, card, {onEdit: cardEdit, onDelete: cardDelete})
        this.parentCard = parentCard
        this.card = card;
        this.onEdit = cardEdit
        this.onDelete = cardDelete
        // this.buttonAtribute = buttonAtribute
        this.cardDivText = document.createElement('div')
        this.cardDiv = document.createElement('div');
    }

    createCardTherapist() {
        const {
            patientFullName,
            id,
            doctor,
        } = this.card
        const cardButtonMore = document.createElement('button')
        const cardEdit = document.createElement('button');
        const cardDelete = document.createElement('button')
        const cardPatientFullName = document.createElement('p');
        const cardDoctor = document.createElement('p');
        const cardId = document.createElement('p');
        const cardButtonDiv = document.createElement('div')
        const {
            cardClassCol, cardClassButtonAll, cardClassButtonMore, cardClassDelete,
            cardClassEdit, cardClassButtonDisplay
        } = this.cardClasses;
        // const {buttonAttribute, buttonAttributeText} = this.buttonAtribute;

        this.cardDiv.classList.add(cardClassCol);
        this.cardDiv.id = id;
        this.parentCard.appendChild(this.cardDiv);

        this.cardDiv.appendChild(this.cardDivText)

        cardPatientFullName.innerHTML = `Фио ${patientFullName}`;
        this.cardDivText.appendChild(cardPatientFullName);

        cardDoctor.innerHTML = `Доктор ${doctor}`;
        this.cardDivText.appendChild(cardDoctor);

        cardButtonDiv.classList.add(cardClassButtonDisplay);
        this.cardDiv.appendChild(cardButtonDiv);

        cardButtonMore.innerHTML = 'Показать больше информации'
        cardButtonMore.classList.add(cardClassButtonAll);
        cardButtonMore.classList.add(cardClassButtonMore)
        // cardButtonMore.setAttribute(buttonAttribute, buttonAttributeText);
        cardButtonDiv.appendChild(cardButtonMore);

        cardEdit.innerHTML = 'Редактировать карточку'
        cardEdit.classList.add(cardClassButtonAll)
        cardEdit.classList.add(cardClassEdit)
        cardEdit.setAttribute('data-toggle', "modal")
        cardEdit.setAttribute('data-target', "#edit-card")
        // cardEdit.setAttribute(buttonAttribute, buttonAttributeText);
        cardButtonDiv.appendChild(cardEdit);

        cardDelete.innerHTML = 'Удалить карточку'
        cardDelete.classList.add(cardClassButtonAll)
        cardDelete.classList.add(cardClassDelete)
        // cardDelete.setAttribute(buttonAttribute, buttonAttributeText);
        cardButtonDiv.appendChild(cardDelete);

        cardButtonMore.addEventListener('click', this.showMoreTherapist.bind(this))
        cardEdit.addEventListener('click', this.editCard.bind(this))
        cardDelete.addEventListener('click', this.deleteCard.bind(this))

        return this.cardDiv
    }

    showMoreTherapist(e) {
        const {
            title,
            description,
            visit,
            age,
        } = this.card
        const cardAge = document.createElement('p');
        const cardTitle = document.createElement('p');
        const cardDescription = document.createElement('p');
        const cardVisit = document.createElement('p');

        cardTitle.innerHTML = `Цель визита ${title}`;
        this.cardDivText.appendChild(cardTitle)

        cardDescription.innerHTML = `Краткое описание ${description}`;
        this.cardDivText.appendChild(cardDescription)

        cardVisit.innerHTML = `Срочность визита ${visit}`;
        this.cardDivText.appendChild(cardVisit)

        cardAge.innerHTML = `Возвраст ${age}`;
        this.cardDivText.appendChild(cardAge)
    }

    editCard(e) {
        this.onEdit(this.card)
    }

    deleteCard(e) {
        const {id} = this.card
        this.cardDiv.remove()

        this.onDelete(id)
    }
}

